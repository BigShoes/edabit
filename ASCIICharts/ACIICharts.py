
def bar_chart(results):
    texte=""
    for value in sorted(results.values(),reverse=True):
        for key in sorted(results.keys()):
            if key not in texte and results[key]==value:
                hastags=""
                if value>0:
                    hastags+="#"*int(value/50)
                    hastags+=" "
                texte+="{}|{}{}\n".format(key,hastags,value)
    return texte[:-1]