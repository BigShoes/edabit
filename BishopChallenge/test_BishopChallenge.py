from unittest import TestCase
from BishopChallenge import bishop

class Test(TestCase):
    def test_unsensor1(self):
        self.assertEqual(bishop("a1", "b4", 2), True)
    def test_unsensor2(self):
        self.assertEqual(bishop("a1", "b5", 5), False)
    def test_unsensor3(self):
        self.assertEqual(bishop("f1", "f1", 0), True)
    def test_unsensor4(self):
        self.assertEqual(bishop("e6", "a1", 2), False)
    def test_unsensor5(self):
        self.assertEqual(bishop("h2", "a2", 1), False)
    def test_unsensor6(self):
        self.assertEqual(bishop("d1", "a3", 1), False)
    def test_unsensor7(self):
        self.assertEqual(bishop("b2", "a4", 2), False)
    def test_unsensor8(self):
        self.assertEqual(bishop("d7", "a5", 0), False)
    def test_unsensor9(self):
        self.assertEqual(bishop("e7", "a6", 2), False)
    def test_unsensor10(self):
        self.assertEqual(bishop("d1", "a7", 1), False)
    def test_unsensor11(self):
        self.assertEqual(bishop("c6", "a8", 2), True)
    def test_unsensor12(self):
        self.assertEqual(bishop("d7", "b1", 1), False)
    def test_unsensor13(self):
        self.assertEqual(bishop("e5", "b2", 2), True)
    def test_unsensor14(self):
        self.assertEqual(bishop("c7", "b3", 0), False)
    def test_unsensor15(self):
        self.assertEqual(bishop("b4", "b4", 1), True)
    def test_unsensor16(self):
        self.assertEqual(bishop("h5", "b5", 1), False)
    def test_unsensor17(self):
        self.assertEqual(bishop("a5", "b6", 1), True)
    def test_unsensor18(self):
        self.assertEqual(bishop("g8", "b7", 2), True)
    def test_unsensor19(self):
        self.assertEqual(bishop("h4", "b8", 1), False)
    def test_unsensor20(self):
        self.assertEqual(bishop("d7", "c1", 2), False)
    def test_unsensor21(self):
        self.assertEqual(bishop("b4", "c2", 1), False)
    def test_unsensor22(self):
        self.assertEqual(bishop("b5", "c3", 1), False)
    def test_unsensor23(self):
        self.assertEqual(bishop("a1", "c4", 2), False)
    def test_unsensor24(self):
        self.assertEqual(bishop("f5", "c5", 1), False)
    def test_unsensor25(self):
        self.assertEqual(bishop("f3", "c6", 1), True)
    def test_unsensor26(self):
        self.assertEqual(bishop("e3", "c7", 0), False)
    def test_unsensor27(self):
        self.assertEqual(bishop("f1", "c8", 0), False)
    def test_unsensor28(self):
        self.assertEqual(bishop("f8", "d1", 1), False)
    def test_unsensor29(self):
        self.assertEqual(bishop("d8", "d2", 0), False)
    def test_unsensor30(self):
        self.assertEqual(bishop("h6", "d3", 1), False)
    def test_unsensor31(self):
        self.assertEqual(bishop("e7", "d4", 1), False)
    def test_unsensor32(self):
        self.assertEqual(bishop("h2", "d5", 1), False)
    def test_unsensor33(self):
        self.assertEqual(bishop("b2", "d6", 1), False)
    def test_unsensor34(self):
        self.assertEqual(bishop("d5", "d7", 1), False)
    def test_unsensor35(self):
        self.assertEqual(bishop("g8", "d8", 0), False)
    def test_unsensor36(self):
        self.assertEqual(bishop("e4", "e1", 1), False)
    def test_unsensor37(self):
        self.assertEqual(bishop("d2", "e2", 1), False)
    def test_unsensor38(self):
        self.assertEqual(bishop("h3", "e3", 1), False)
    def test_unsensor39(self):
        self.assertEqual(bishop("e3", "e4", 2), False)
    def test_unsensor40(self):
        self.assertEqual(bishop("a7", "e5", 1), False)
    def test_unsensor41(self):
        self.assertEqual(bishop("d1", "e6", 2), True)
    def test_unsensor42(self):
        self.assertEqual(bishop("d6", "e7", 0), False)
    def test_unsensor43(self):
        self.assertEqual(bishop("c5", "e8", 1), False)
    def test_unsensor44(self):
        self.assertEqual(bishop("c5", "f1", 2), False)
    def test_unsensor45(self):
        self.assertEqual(bishop("d3", "f2", 1), False)
    def test_unsensor46(self):
        self.assertEqual(bishop("h3", "f3", 1), False)
    def test_unsensor47(self):
        self.assertEqual(bishop("h1", "f4", 1), False)
    def test_unsensor48(self):
        self.assertEqual(bishop("h3", "f5", 1), True)
    def test_unsensor49(self):
        self.assertEqual(bishop("b2", "f6", 0), False)
    def test_unsensor50(self):
        self.assertEqual(bishop("b8", "f7", 1), False)
    def test_unsensor51(self):
        self.assertEqual(bishop("e5", "f8", 2), True)
    def test_unsensor52(self):
        self.assertEqual(bishop("d7", "g1", 0), False)
    def test_unsensor53(self):
        self.assertEqual(bishop("b6", "g2", 2), False)
    def test_unsensor54(self):
        self.assertEqual(bishop("e1", "g3", 2), True)
    def test_unsensor55(self):
        self.assertEqual(bishop("c3", "g4", 1), False)
    def test_unsensor56(self):
        self.assertEqual(bishop("d3", "g5", 1), False)
    def test_unsensor57(self):
        self.assertEqual(bishop("d6", "g6", 2), False)
    def test_unsensor58(self):
        self.assertEqual(bishop("a3", "g7", 1), False)
    def test_unsensor59(self):
        self.assertEqual(bishop("h3", "g8", 1), False)
    def test_unsensor60(self):
        self.assertEqual(bishop("g6", "h1", 1), False)
    def test_unsensor61(self):
        self.assertEqual(bishop("g7", "h2", 1), False)
    def test_unsensor62(self):
        self.assertEqual(bishop("c4", "h3", 1), False)
    def test_unsensor63(self):
        self.assertEqual(bishop("a3", "h4", 2), True)
    def test_unsensor64(self):
        self.assertEqual(bishop("d5", "h5", 2), True)
    def test_unsensor65(self):
        self.assertEqual(bishop("a2", "h6", 1), False)
    def test_unsensor66(self):
        self.assertEqual(bishop("b1", "h7", 1), True)
    def test_unsensor67(self):
        self.assertEqual(bishop("f1", "h8", 1), False)