def uncensor(txt, vowels):
    while '*' in txt:
        txt = "{}{}{}".format(txt[:txt.find('*')],vowels[0],txt[txt.find('*')+1:])
        vowels = vowels[1:]
    return txt
