from unittest import TestCase
from CensoredStrings import uncensor

class Test(TestCase):
    def test_unsensor1(self):
        self.assertEquals(uncensor('Wh*r* d*d my v*w*ls g*?', 'eeioeo'), 'Where did my vowels go?')
    def test_unsensor2(self):
        self.assertEquals(uncensor('abcd', ''), 'abcd', 'Works with no vowels.')
    def test_unsensor3(self):
        self.assertEquals(uncensor('*PP*RC*S*', 'UEAE'), 'UPPERCASE', 'Works with uppercase')
    def test_unsensor4(self):
        self.assertEquals(uncensor('Ch**s*, Gr*mm*t -- ch**s*', 'eeeoieee'), 'Cheese, Grommit -- cheese',
                       'Works with * at the end')
    def test_unsensor5(self):
        self.assertEquals(uncensor('*l*ph*nt', 'Eea'), 'Elephant', 'Works with * at the start')