def crop_hydrated(field):
    Xrange = range(len(field[0]))
    Yrange = range(len(field))
    for y in Yrange:
        for x in Xrange:
            if field[y][x]=='c':
                isHydrated=False
                for yMove in [y-1,y,y+1]:
                    for xMove in [x-1,x,x+1]:
                        if yMove in Yrange and xMove in Xrange and field[yMove][xMove]=='w':
                            isHydrated=True
                if not isHydrated:
                    return False
    return True
