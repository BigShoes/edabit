from unittest import TestCase
from CropFields import crop_hydrated

class Test(TestCase):

    def test_crophydrated1(self):
        self.assertEquals(crop_hydrated([
  [ "w", "w" ],
  [ "w", "c" ],
  [ "c", "c" ],
  [ "c", "w" ],
  [ "c", "w" ]
]), True)

    def test_crophydrated2(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w", "w", "w", "c" ],
  [ "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "w", "c" ]
]), True)

    def test_crophydrated3(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w" ]
]), True)

    def test_crophydrated4(self):
        self.assertEquals(crop_hydrated([
  [ "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "w", "c" ]
]), True)

    def test_crophydrated5(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "c" ],
  [ "w", "w", "c" ],
  [ "c", "c", "c" ],
  [ "w", "w", "c" ],
  [ "c", "c", "c" ],
  [ "c", "c", "c" ],
  [ "c", "w", "c" ]
]), True)

    def test_crophydrated6(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "c" ],
  [ "w", "w", "c" ]
]), True)

    def test_crophydrated7(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w", "w", "c", "c", "w", "c" ]
]), True)

    def test_crophydrated8(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w", "c", "c", "w", "w" ],
  [ "c", "c", "w", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "w" ],
  [ "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "w", "w" ]
]), True)

    def test_crophydrated9(self):
        self.assertEquals(crop_hydrated([
  [ "c" ],
  [ "w" ],
  [ "c" ],
  [ "c" ],
  [ "w" ],
  [ "c" ]
]), True)

    def test_crophydrated10(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "w", "w", "c", "c", "c" ],
  [ "c", "w", "c", "w", "w", "c", "w" ],
  [ "w", "w", "c", "w", "c", "c", "c" ]
]), True)


    def test_crophydrated11(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "w", "c", "c", "w", "c", "w" ]
]), False)

    def test_crophydrated12(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "c", "c", "c", "w", "c" ],
  [ "c", "w", "c", "c", "w", "c", "w" ],
  [ "c", "c", "c", "w", "c", "w", "c" ],
  [ "w", "w", "c", "c", "c", "c", "c" ],
  [ "c", "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "w", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "w" ]
]), False)

    def test_crophydrated13(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w", "c", "c" ],
  [ "w", "c", "c", "c" ],
  [ "c", "c", "c", "c" ],
  [ "w", "c", "c", "c" ],
  [ "w", "w", "c", "c" ],
  [ "c", "w", "c", "c" ],
  [ "c", "c", "w", "c" ],
  [ "c", "c", "w", "w" ],
  [ "c", "c", "c", "w" ]
]), False)

    def test_crophydrated14(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "w", "c" ],
  [ "c", "w", "w", "c", "c", "c" ]
]), False)

    def test_crophydrated15(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "c", "c", "c", "w", "c" ],
  [ "w", "c", "c", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "w", "w", "c", "c", "w", "w" ],
  [ "c", "c", "w", "c", "w", "c", "c" ],
  [ "w", "c", "c", "c", "w", "c", "c" ],
  [ "c", "c", "c", "c", "w", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "c" ]
]), False)

    def test_crophydrated16(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w", "c", "c", "w", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "w", "w", "w", "c" ],
  [ "w", "c", "c", "w", "w", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c", "c" ]
]), False)

    def test_crophydrated17(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "w", "c", "w" ],
  [ "c", "c", "c", "c", "c" ],
  [ "w", "c", "w", "c", "c" ],
  [ "c", "w", "w", "c", "c" ],
  [ "c", "c", "c", "c", "w" ],
  [ "c", "c", "c", "w", "c" ]
]), False)

    def test_crophydrated18(self):
        self.assertEquals(crop_hydrated([
  [ "c", "w", "c", "c", "c", "w", "w", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "w", "c" ]
]), False)

    def test_crophydrated19(self):
        self.assertEquals(crop_hydrated([
  [ "w", "w", "c", "c", "w" ],
  [ "c", "c", "c", "c", "c" ],
  [ "c", "c", "w", "c", "c" ],
  [ "w", "c", "c", "w", "w" ],
  [ "c", "c", "w", "c", "c" ],
  [ "c", "c", "w", "c", "c" ],
  [ "c", "c", "c", "w", "c" ]
]), False)

    def test_crophydrated20(self):
        self.assertEquals(crop_hydrated([
  [ "c", "c", "w", "c", "c", "w" ],
  [ "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "w", "c", "c" ],
  [ "c", "c", "c", "c", "w", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ]
]), False)

# By Harith Shah