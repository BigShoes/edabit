from unittest import TestCase
from EncodeMorse import encode_morse

class Test(TestCase):
    def test_encode_morse1(self):
        self.assertEquals(encode_morse("EDABBIT CHALLENGE"), ". -.. .- -... -... .. -   -.-. .... .- .-.. .-.. . -. --. .")
    def test_encode_morse2(self):
        self.assertEquals(encode_morse("HELP ME !"), ".... . .-.. .--.   -- .   -.-.--")
    def test_encode_morse3(self):
        self.assertEquals(encode_morse("CHALLENGE"), "-.-. .... .- .-.. .-.. . -. --. .")
    def test_encode_morse4(self):
        self.assertEquals(encode_morse("1 APPLE AND 5 CHERRY, 7 SANDWICHES, 2 TABLES, 9 INVITED GUYS ! THAT'S SO COOL..."),
                       ".----   .- .--. .--. .-.. .   .- -. -..   .....   -.-. .... . .-. .-. -.-- --..--   --...   ... .- -. -.. .-- .. -.-. .... . ... --..--   ..---   - .- -... .-.. . ... --..--   ----.   .. -. ...- .. - . -..   --. ..- -.-- ...   -.-.--   - .... .- - .----. ...   ... ---   -.-. --- --- .-.. .-.-.- .-.-.- .-.-.-")
    def test_encode_morse5(self):
        self.assertEquals(encode_morse("did you got my mail ?"),
                       "-.. .. -..   -.-- --- ..-   --. --- -   -- -.--   -- .- .. .-..   ..--..")
    def test_encode_morse6(self):
        self.assertEquals(encode_morse("TWO THInGS TO KNOW : i FORGeT THeM :C"),
                       "- .-- ---   - .... .. -. --. ...   - ---   -.- -. --- .--   ---...   ..   ..-. --- .-. --. . -   - .... . --   ---... -.-.")