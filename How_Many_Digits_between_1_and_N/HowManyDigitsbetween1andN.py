def digits(number:str)-> int:
    i = 1
    return_value = 0
    while i<number:
        return_value+=len(str(i))
        i+=1
    return return_value