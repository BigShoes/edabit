from unittest import TestCase
from HowManyDigitsbetween1andN import *

class Test(TestCase):
    def test_digits1(self):
        self.assertEquals(digits(1), 0)
    def test_digits2(self):
        self.assertEquals(digits(10), 9)
    def test_digits3(self):
        self.assertEquals(digits(100), 189)
    def test_digits4(self):
        self.assertEquals(digits(2020), 6969)
    def test_digits5(self):
        self.assertEquals(digits(103496754), 820359675)
    def test_digits6(self):
        self.assertEquals(digits(3248979384), 31378682729)
    def test_digits7(self):
        self.assertEquals(digits(122398758003456), 1724870258940729)
    def test_digits8(self):
        self.assertEquals(digits(58473029386609125789), 1158349476621071404669)