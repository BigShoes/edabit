'''
@version : 2.7
@author : jean loup aurisset
Fonction qui pour une grille passee en paramettre
retourne cette meme grille ou pour chaque case apparait le
nombre de "#' adjacent
-----------------------------------------------------------
remplace tous les '-' par des 0 dans la grille
creation une liste position qui donne l'etendue des positions possibles
pour y dans ces positions
    pour x dans ces positions
        si cette case est un #
        alors
            pour un nouveau y de y-1 à y+1
                et pour un nouveau x de x-1 à x+1
                    si ces deux variables sont bien dans les positions admises
                    et que ce ne sont pas eux meme des #
                    alors
                        ajoute 1 a cette position

La fonction ajoute 1 a toutes les positions ajacentes a un #
'''
import pprint
def num_grid(lst):
    lst = [[ c.replace('-','0') for c in xRow ] for xRow in lst]
    positions = range(len(lst))
    for y in positions:
        for x in positions:
            if lst[y][x] == "#":
                for yMove in [y-1,y,y+1]:
                    for xMove in [x-1,x,x+1]:
                        if yMove in positions and xMove in positions and lst[yMove][xMove] != "#":
                            lst[yMove][xMove]= str(int(lst[yMove][xMove])+1)
    return lst