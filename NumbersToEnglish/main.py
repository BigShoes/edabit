def num_to_eng(num):
    num_dict = {"0": "zero",
                "1" : "one",
                "2" : "two",
                "3" : "three",
                "4" : "four",
                "5" : "five",
                "6" : "six",
                "7" : "seven",
                "8" : "eight",
                "9" : "nine"}
    # "teen" rule from 13 -> 19
    exception_dict = {"10" : "ten",
                      "11" : "eleven",
                      "12" : "twelve"}
    tenth_dict = {"2" : "twenty",
                  "3" : "thirty",
                  "4" : "fourty"}
    return "O"