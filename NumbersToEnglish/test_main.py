from unittest import TestCase

from NumbersToEnglish.main import num_to_eng


class Test(TestCase):
    def test_num_to_eng(self):
        self.assertEquals(num_to_eng(0), "zero")
    def test_num_to_eng1(self):
        self.assertEquals(num_to_eng(26), "twenty six")
    def test_num_to_eng2(self):
        self.assertEquals(num_to_eng(519), "five hundred nineteen")
    def test_num_to_eng3(self):
        self.assertEquals(num_to_eng(106), "one hundred six")
    def test_num_to_eng4(self):
        self.assertEquals(num_to_eng(999), "nine hundred ninety nine")