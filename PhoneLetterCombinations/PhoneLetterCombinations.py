def letter_combinations(digits):
    keyBoard = {"2":"abc","3":"def","4":"ghi","5":"jkl","6":"mno","7":"pqrs","8":"tuv","9":"wxyz"}
    combinaisons=[""]
    for number in digits:
        new_combinaison=[]
        for letter in keyBoard[number]:
            new_combinaison += [ c + letter for c in combinaisons]
        combinaisons = new_combinaison
    return sorted(combinaisons)