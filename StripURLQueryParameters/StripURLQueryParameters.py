def strip_url_params(url, params_to_strip=""):
    if "?" in url:
        parameters = { param[0] : param[2] for param in url[19:].split('&') if param[0] not in params_to_strip}
        return "https://edabit.com?" + "&".join( "{}={}".format(key,parameters[key]) for key in sorted(parameters.keys()) )
    return "https://edabit.com"