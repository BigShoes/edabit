from unittest import TestCase
from StripURLQueryParameters import strip_url_params

class Test(TestCase):
    def test_strip_url_params1(self):
        self.assertEquals(strip_url_params("https://edabit.com?a=1&b=2&a=2"), "https://edabit.com?a=2&b=2")
    def test_strip_url_params2(self):
        self.assertEquals(strip_url_params("https://edabit.com?a=1&b=2&a=2", ["b"]), "https://edabit.com?a=2")
    def test_strip_url_params3(self):
        self.assertEquals(strip_url_params("https://edabit.com", ["b"]), "https://edabit.com")
    def test_strip_url_params4(self):
        self.assertEquals(strip_url_params("https://edabit.com?a=1&b=2"), "https://edabit.com?a=1&b=2")
    def test_strip_url_params5(self):
        self.assertEquals(strip_url_params("https://edabit.com?a=1&b=2", ["c"]), "https://edabit.com?a=1&b=2")
    def test_strip_url_params6(self):
        self.assertEquals(strip_url_params("https://edabit.com?a=1&b=2&c=3&d=4", ["a", "d"]), "https://edabit.com?b=2&c=3", "The 2nd argument can contain multiple URL params.")
    def test_strip_url_params7(self):
        self.assertEquals(strip_url_params("https://edabit.com?a=1&b=2&c=3&d=4&c=5", ["a", "d"]), "https://edabit.com?b=2&c=5")