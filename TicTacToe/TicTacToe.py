is_win = lambda ligne,board : board[ligne[0][0]][ligne[0][1]] == board[ligne[1][0]][ligne[1][1]] and board[ligne[1][0]][ligne[1][1]] == board[ligne[2][0]][ligne[2][1]]

def tic_tac_toe(board):
    possibilities = [((0,0),(1,1),(2,2)),((0,2),(1,1),(2,0))]
    for position in range(3):
        possibilities+=[((0,position),(1,position),(2,position))]
        possibilities+=[((position,0),(position,1),(position,2))]
    winner = [board[ligne[0][0]][ligne[0][1]] for ligne in possibilities if is_win(ligne, board)]
    winner = filter(is_win,)
    return winner[0] if len(winner)>0 else "Draw"