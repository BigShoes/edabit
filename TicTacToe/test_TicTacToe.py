from unittest import TestCase
from TicTacToe import tic_tac_toe

class Test(TestCase):
    def test_tictactoe1(self):
        self.assertEquals(tic_tac_toe([
        ["X", "O", "X"],
        ["O", "X", "O"],
        ["O", "X", "X"]
    ]), "X")

    def test_tictactoe2(self):
        self.assertEquals(tic_tac_toe([
        ["O", "O", "O"],
        ["O", "X", "X"],
        ["E", "X", "X"]
    ]), "O")

    def test_tictactoe3(self):
        self.assertEquals(tic_tac_toe([
        ["X", "X", "O"],
        ["O", "O", "X"],
        ["X", "X", "O"]
    ]), "Draw")

    def test_tictactoe4(self):
        self.assertEquals(tic_tac_toe([
        ["X", "O", "O"],
        ["X", "O", "O"],
        ["X", "X", "X"]
    ]), "X")

    def test_tictactoe5(self):
        self.assertEquals(tic_tac_toe([
        ["X", "X", "O"],
        ["O", "O", "X"],
        ["X", "X", "O"]
    ]), "Draw")

    def test_tictactoe6(self):
        self.assertEquals(tic_tac_toe([
        ["X", "O", "X"],
        ["O", "X", "O"],
        ["E", "E", "X"]
    ]), "X")

    def test_tictactoe7(self):
        self.assertEquals(tic_tac_toe([
        ["X", "O", "E"],
        ["X", "O", "E"],
        ["E", "O", "X"]
    ]), "O")

    # By Harith Shah