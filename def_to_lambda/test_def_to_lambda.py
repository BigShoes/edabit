import unittest
from def_to_lambda.main import lambda_to_def


class Test_lambda_to_def(unittest.TestCase):
    def test_something1(self):
        self.assertEqual(
            lambda_to_def("func = lambda a, b: a * (b - 2)"),
            "def func(a, b):\n\treturn a * (b - 2)"
        )

    def test_something2(self):
        self.assertEqual(
            lambda_to_def("func = lambda w: w + 'lambda'"),
            "def func(w):\n\treturn w + 'lambda'"
        )

    def test_something3(self):
        self.assertEqual(
            lambda_to_def("a = lambda x, y=2: x**y"),
            "def a(x, y=2):\n\treturn x**y"
        )

    def test_something4(self):
        self.assertEqual(
            lambda_to_def("z = lambda lambdadef: lambdadef.split(':')"),
            "def z(lambdadef):\n\treturn lambdadef.split(':')"
        )

    def test_something5(self):
        self.assertEqual(
            lambda_to_def("t = lambda s='t = lambda s: s + 1': s + 's'"),
            "def t(s='t = lambda s: s + 1'):\n\treturn s + 's'"
        )
