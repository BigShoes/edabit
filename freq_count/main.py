def deserialized(tree) -> list():
    """
    Fonction de désérialisation de l'arbre
    """
    deserialized_tree = []
    for leaf in tree:
        if len(leaf) == 2 and type(leaf[0]) == int and type(leaf[1]) == int:
            deserialized_tree += [leaf]
        else:
            if len(leaf)>0:
                deserialized_tree += deserialized(leaf)
    return deserialized_tree


def freq_count(lst, el, level=0) -> list():
    """
    Objectif retourner le nombre d'elements el dans la liste lst pour chacun des niveaux
    :param level int:
    :param lst list:
    :param el int:
    :return list:
    """
    nb_elements = lst.count(el)
    layers = [freq_count(layer, el, level + 1) for layer in lst if type(layer) == type(lst)]
    if level == 0:
        deserialized_tree = deserialized([[level, nb_elements], layers])
        depth = max([element[0] for element in deserialized_tree])
        colapsed = [[level, 0] for level in range(0, depth+1)]
        for leaf in deserialized_tree:
            colapsed[leaf[0]][1] += leaf[1]
        return colapsed
    if len(layers) != 0:
        return [[level, nb_elements], layers]
    else:
        return [level, nb_elements]
