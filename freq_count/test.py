import unittest
from freq_count.main import freq_count


class MyTestCase(unittest.TestCase):
    def test_something1(self):
        self.assertEqual(freq_count([1, 1, 1, 1], 1), [[0, 4]])
    def test_something2(self):
        self.assertEqual(freq_count([1, 1, 2, 2], 1), [[0, 2]])
    def test_something3(self):
        self.assertEqual(freq_count([1, 1, 2, [1]], 1), [[0, 2], [1, 1]])
    def test_something4(self):
        self.assertEqual(freq_count([1, 1, 2, [[1]]], 1), [[0, 2], [1, 0], [2, 1]])
    def test_something5(self):
        self.assertEqual(freq_count([[[1]]], 1), [[0, 0], [1, 0], [2, 1]])
    def test_something6(self):
        self.assertEqual(freq_count([1, 4, 4, [1, 1, [1, 2, 1, 1]]], 1), [[0, 1], [1, 2], [2, 3]])
    def test_something7(self):
        self.assertEqual(freq_count([1, 5, 5, [5, [1, 2, 1, 1], 5, 5], 5, [5]], 5), [[0, 3], [1, 4], [2, 0]])
    def test_something8(self):
        self.assertEqual(freq_count([1, [2], 1, [[2]], 1, [[[2]]], 1, [[[[2]]]]], 2), [[0, 0], [1, 1], [2, 1], [3, 1], [4, 1]])

