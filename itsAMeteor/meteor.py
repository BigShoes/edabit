
def will_hit(equation, position)->bool:
    '''
    @param equation :str:   y = a*x + b
    @param position :tuple: (X,Y)
    '''
    part2 = equation[4:]
    x= position[0]
    y= position[1]
    to_eval = part2.split("x")[0]+"* x "+part2.split("x")[1]
    return y == eval(to_eval)