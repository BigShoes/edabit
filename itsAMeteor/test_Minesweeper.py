from unittest import TestCase
from meteor import will_hit
class Test(TestCase):
    def test_num_grid1(self):
        self.assertEquals(will_hit("y = 2x - 5", (0, 0)), False)
    def test_num_grid2(self):
        self.assertEquals(will_hit("y = -4x + 6", (1, 2)), True)
    def test_num_grid3(self):
        self.assertEquals(will_hit("y = -4x + 6", (2, 2)), False)
    def test_num_grid4(self):
        self.assertEquals(will_hit("y = 3x - 8", (4, 4)), True)
    def test_num_grid5(self):
        self.assertEquals(will_hit("y = 2x + 6", (3, 2)), False)
    def test_num_grid6(self):
        self.assertEquals(will_hit("y = -3x + 15", (5, 0)), True)